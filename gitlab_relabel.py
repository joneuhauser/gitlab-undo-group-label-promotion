from datetime import datetime
import os
import shutil
import pickle
import conf
import gitlab
import re


gl = gitlab.Gitlab(private_token = conf.api_key)

# main group 
group = gl.groups.get(conf.group)

def find_subprojects_recursively(parent):
    if parent == group:
        print("Enumerating projects...")
    result = []
    subgroups = parent.subgroups.list()
    for subgroup in subgroups:
        result += find_subprojects_recursively(gl.groups.get(subgroup.id))
    projects = parent.projects.list()
    for project in projects:
        print(f"Found project {str(project.id):8} at {project.path_with_namespace}, subproject of {parent.name}")
        result += [project.id]
    return set(result)

all_projects = find_subprojects_recursively(group)
print(f"Success! {len(all_projects)} projects found.")

# now figure out which labels we want to delete
def separate_labels(parent, delete_regex):
    labels_to_keep = []
    labels_to_delete = []
    for label in parent.labels.list(all=True):
        if "LPE" in label.name:
            print("hi")
        if re.match(f".*{delete_regex}.*", label.name):
            labels_to_delete += [label]
        else:
            labels_to_keep += [label]
    print(f"Located {len(labels_to_delete)} labels to delete:{chr(10)}  {f'{chr(10)}  '.join([f'{i.name} ({i.id})' for i in labels_to_delete])}")
    print(f"The following {len(labels_to_keep)  } labels will be kept as group labels:  {chr(10)}  {f'{chr(10)}  '.join([f'{i.name} ({i.id})' for i in labels_to_keep])}")

    return labels_to_delete

if conf.label_regex is None:
    labels_to_delete = group.labels.list(all=True)
else:
    labels_to_delete = separate_labels(group, conf.label_regex)


# Save all issues and merge requests
def find_issues(projects):
    issues = [] # project id, issue id, issue url, title, labels, updated at
    for project_id in projects:
        project = gl.projects.get(project_id)
        all_issues = project.issues.list(all=True)
        for issue in all_issues:
            issues += [[project_id, issue.iid, issue.web_url, issue.title, issue.labels, issue.updated_at]]
        try:
            all_mrs = project.mergerequests.list(all=True)
            for mr in all_mrs:
                issues += [[project_id, mr.iid, mr.web_url, mr.title, mr.labels, mr.updated_at]]
        except gitlab.GitlabListError:
            pass # some projects don't have merge requests (no code) 
    return issues

if conf.RELOAD_ISSUES:
    issues = find_issues(all_projects)
    # We save the issue list and the labels in data files, so if anything goes wrong, we still have all the information.
    t = datetime.now().strftime("%Y%m%d-%H%M%S")
    if os.path.exists("issues.dat"):
        shutil.move("issues.dat", f"issues{t}.dat")
    if os.path.exists("labels.dat"):
        shutil.move("labels.dat", f"labels{t}.dat")

    with open('issues.dat','wb') as f:
        pickle.dump(issues, f)
    with open('labels.dat','wb') as f:
        pickle.dump([[i.id, i.name, i.color, i.description] for i in labels_to_delete], f)
    labels_to_delete_dict = {}
    for label in labels_to_delete:
        labels_to_delete_dict[label.name] = [label.color, label.description] 
else:
    with open("issues.dat", "rb") as f:
        issues = pickle.load(f)
    with open("labels.dat", "rb") as f:
        labels = pickle.load(f)
        labels_to_delete_dict = {}
        for label in labels:
            labels_to_delete_dict[label[1]] = [label[2], label[3]] 
        labels_to_delete = []



labels_to_recreate = {}
for p in all_projects:
    p_labels = set(x for xs in [i[4] for i in issues if i[0] == p] for x in xs)
    p_labels = p_labels.intersection(set(labels_to_delete_dict.keys())) #only group labels
    labels_to_recreate[p] = p_labels
    print(f"Used group labels in project {gl.projects.get(p).name}: {p_labels}")

if conf.EDIT:
    # delete the original labels
    for label in labels_to_delete:
        try:
            label.delete()
        except:
            # Label has been deleted in a previous run
            pass
    # and recreate them
    for p in all_projects:
        for label in labels_to_recreate[p]:
            try:
                print(f"Recreating label {label} in project {gl.projects.get(p).web_url}")
                gl.projects.get(p).labels.create({"name" : label, "color": labels_to_delete_dict[label][0], "description": labels_to_delete_dict[label][1]})
            except:
                print("Label probably already exists")
issues_sorted = sorted(issues, key=lambda x: x[5])

def get_issue_object(row):
    if "merge_requests" in row[2]:
        iss = gl.projects.get(row[0]).mergerequests.get(row[1])
    else:
        iss = gl.projects.get(row[0]).issues.get(row[1])
    return iss
for index, issue in enumerate(issues_sorted):
    try:
        if len(issue[4]) == 0:
            print(f'[{index}/{len(issues)}] Skipping issue {issue[2].replace("https://gitlab.com", "")}, no labels')
            continue
        iss = get_issue_object(issue)
        assert(iss.web_url == issue[2])
        if conf.EDIT:
            print(f'[{index}/{len(issues)}] Processing issue {issue[2].replace("https://gitlab.com", "")}, and setting labels {issue[4]}. Timestamp: {issue[5]}' )
            iss.labels = issue[4]
            #iss.updated_at = issue[5] # this doesn't work
            iss.save()
    except Exception as err:
        # catch any exception in this stage, we want to keep processing.
        print(f'An exception occurred during processing of issue  {issue[2].replace("https://gitlab.com", "")}: {err}')
        with open("error.txt", "a") as file:
            file.write(f'An exception occurred during processing of issue  {issue[2]}: {err}\n')


    





