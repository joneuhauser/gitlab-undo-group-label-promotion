api_key = "abc" # api key must have write access to the repo

group = 12345 # group ID

label_regex = None # If not none, un-promote only the labels that match this regex.

RELOAD_ISSUES = True # Whether to pull an updated list of issues and MRs, which includes the following information: 
                     # project id, issue / MR id, URL, title, labels and timestamp
                     # This information is saved in "issues.dat" as a picked file. If it exists, the old file is moved to issues_{current-timestamp}.dat,
                     # so no information should be lost.
                     # If false, the existing issues.dat file is used.
                     # Labels are always pulled from the server when the script is used.
                     # Setting this to false is useful if the previous run was not successful for some reason.

EDIT = True          # Whether to edit the issues / mrs / labels
