# Gitlab Group label "Un-promote"

Highly experimental. It is possible that you permanently lose information if you use this tool. Use on your own risk!
{: .alert .alert-warning}

## What does the script do?

Once a label has been promoted to a group label on Gitlab, there is no way back. But now that label pollutes the list of available label in every subproject.

This script does the un-promotion manually, with the following targets:
 - only keep the labels in those groups that actually use them
 - try to be as unintrusive as possible: all issues and MRs should keep their labels, and all discussions that reference labels should keep them. Ideally, preserve the "Last Edited" timestamp, if that's not possible, at least keep the order correct.

## Details

 - Starting from a group (`conf.group`), all subprojects (possibly nested) are located.
 - The labels of the group are fetched and filtered; only those that match `conf.delete_regex` will be un-promoted. (If the parameter is none, all group labels are unpromoted.)
 - For each project, the list of issues and merge requests is fetched. The most important information, i.e. their identification, labels and last edited timestamp, is saved in a picked file. This file is never overwritten, only moved. So if something goes wrong, here's a backup of the most important information.
 - Using this list, find out which projects need which labels from now on. Delete the group labels and recreate the labels on project level.
 - Walk through all issues, starting from the on that has been changed the earliest; set the labels and set the last-edited timestamp. Same for merge request, although here setting the last-edited timestamp has no effect, sadly.

 ## Caveats

 - The Gitlab python API doesn't expose the label_details parameter of the issues request, so we only compare label strings, not label IDs. Since Gitlab prevents duplicate label strings, this should be ok.
 